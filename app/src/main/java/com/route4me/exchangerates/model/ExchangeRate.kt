package com.route4me.exchangerates.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ExchangeRate(val name: String, val value: Double, val sign: String) {

    @PrimaryKey(autoGenerate = false)
    var rateName: String = ""
    var rateValue: Double = 0.0
    var rateSign: String = ""

    init {
        this.rateName = name
        this.rateValue = value
        this.rateSign = sign
    }

}