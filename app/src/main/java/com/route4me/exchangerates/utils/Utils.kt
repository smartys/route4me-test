package com.route4me.exchangerates.utils

import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AlertDialog
import java.util.*

class Utils {
    companion object {
        const val APP_SHARED_PREFS = "RATES_PREFS"
        const val REFRESH_TIME_KEY = "REFRESH_TIME"
        const val REFRESH_TIME_VALUE = 600000

        fun getCurrencySignBy(currencyName: String): String {
            var symbol = ""
            val currency = Currency.getInstance(currencyName)
            if (currency != null) {
                symbol = currency.getSymbol()
            }
            return symbol
        }

        fun createDialog(activityContext: Context, dialogText: String, withButton: Boolean = true): Dialog {
            val dialogBuilder = AlertDialog.Builder(activityContext)
            dialogBuilder.setMessage(dialogText)
                .setCancelable(false)
            if (withButton) {
                dialogBuilder.setPositiveButton("Ok") { dialog, id ->
                    dialog.dismiss()
                }
            }
            return dialogBuilder.create()
        }
    }
}