package com.route4me.exchangerates

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.route4me.exchangerates.db.ExchangeRatesDB
import com.route4me.exchangerates.ui.fragments.ExchangeRatesChartFragment
import com.route4me.exchangerates.ui.fragments.ExchangeRatesListFragment

class MainActivity : AppCompatActivity(), ExchangeRatesListFragment.ExchangeRateClickListener {

    companion object {
        const val DATA_BUNDLE_CURRENCY_NAME = "CUR_NAME"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ExchangeRatesDB.initDB(this)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, ExchangeRatesListFragment.newInstance())
            .addToBackStack(ExchangeRatesListFragment.TAG)
            .commit()
    }

    override fun onItemClicked(ratesName: String) {
        val bundle = Bundle()
        bundle.putString(DATA_BUNDLE_CURRENCY_NAME, ratesName)
        val ratesChartFragment = ExchangeRatesChartFragment.newInstance()
        ratesChartFragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, ratesChartFragment)
            .addToBackStack(ExchangeRatesChartFragment.TAG)
            .commit()
    }

}
