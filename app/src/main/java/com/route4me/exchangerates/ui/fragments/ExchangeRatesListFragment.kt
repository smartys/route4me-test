package com.route4me.exchangerates.ui.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.route4me.exchangerates.R
import com.route4me.exchangerates.api.ExchangeRatesAPI
import com.route4me.exchangerates.db.ExchangeRatesDB
import com.route4me.exchangerates.model.ExchangeRate
import com.route4me.exchangerates.ui.adapter.ExchangeRatesListAdapter
import com.route4me.exchangerates.utils.Utils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.rates_list_fragment.*
import java.util.*
import kotlin.collections.ArrayList

class ExchangeRatesListFragment: ExchangeRatesBaseFragment() {

    interface ExchangeRateClickListener {
        fun onItemClicked(ratesName: String)
    }

    companion object {
        val TAG = "ExchangeRatesListFragment"
        fun newInstance(): ExchangeRatesListFragment = ExchangeRatesListFragment()
    }

    private lateinit var mCallback: ExchangeRateClickListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mCallback = context as ExchangeRateClickListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString()
                    + " must implement item click listener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.rates_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val ratesListAdapter = ExchangeRatesListAdapter(emptyList(), mCallback)
        ratesRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ratesListAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
        loadRates(ratesListAdapter)
    }

    private fun loadRates(listAdapter: ExchangeRatesListAdapter) {
        val sharedPrefs = activity!!.getSharedPreferences(Utils.APP_SHARED_PREFS, Context.MODE_PRIVATE)
        val refreshTime = sharedPrefs.getLong(Utils.REFRESH_TIME_KEY, 0L)
        val calendar = Calendar.getInstance()
        val now = calendar.timeInMillis
        if (refreshTime > 0L && now - refreshTime < Utils.REFRESH_TIME_VALUE) {
            disposables.add(Observable.fromCallable { ExchangeRatesDB.ratesDao?.getRates() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { ratesList ->
                        listAdapter.update(ratesList)
                    },
                    { error ->
                        Log.e("Error", "### err: " + error)
                    }))
        } else {
            disposables.add(ExchangeRatesAPI.getLatestRates()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { ratesList ->
                        if (ratesList.isEmpty()) {
                            Utils.createDialog(activity!!, "Unable to load rates data, please, check your Internet connection and restart app.")
                        } else {
                            listAdapter.update(ratesList)
                            for (rate in ratesList) {
                                ExchangeRatesDB.ratesDao?.insertRate(rate)
                            }
                        }
                    },
                    { error ->
                        Log.e("Error", "### err: " + error)
                    }
                )
            )
        }
    }
}