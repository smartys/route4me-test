package com.route4me.exchangerates.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.route4me.exchangerates.R
import com.route4me.exchangerates.model.ExchangeRate
import com.route4me.exchangerates.ui.fragments.ExchangeRatesListFragment

class ExchangeRatesListAdapter(private var list: List<ExchangeRate>, private val clickListener: ExchangeRatesListFragment.ExchangeRateClickListener)
    : RecyclerView.Adapter<RatesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RatesViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: RatesViewHolder, position: Int) {
        val exRate: ExchangeRate = list[position]
        holder.bind(exRate, clickListener)
    }

    override fun getItemCount(): Int = list.size

    fun update(ratesList: List<ExchangeRate>) {
        list = ratesList
        notifyDataSetChanged()
    }

}

class RatesViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.rates_list_item, parent, false)) {
    private var mRateTitleView: TextView? = null
    private var mRateValueView: TextView? = null
    private var mRateSignView: TextView? = null

    init {
        mRateTitleView = itemView.findViewById(R.id.rate_title)
        mRateValueView = itemView.findViewById(R.id.rate_value)
        mRateSignView = itemView.findViewById(R.id.rate_sign)
    }

    fun bind(exRate: ExchangeRate, callback: ExchangeRatesListFragment.ExchangeRateClickListener) {
        mRateTitleView?.text = exRate.rateName
        mRateValueView?.text = String.format("%.2f", exRate.rateValue)
        mRateSignView?.text = exRate.rateSign

        itemView.setOnClickListener { callback.onItemClicked(exRate.rateName) }
    }

}