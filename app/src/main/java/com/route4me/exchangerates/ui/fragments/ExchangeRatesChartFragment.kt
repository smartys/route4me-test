package com.route4me.exchangerates.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.route4me.exchangerates.MainActivity.Companion.DATA_BUNDLE_CURRENCY_NAME
import com.route4me.exchangerates.api.ExchangeRatesAPI
import com.route4me.exchangerates.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.components.YAxis.AxisDependency
import com.github.mikephil.charting.data.LineDataSet
import android.graphics.Color
import com.github.mikephil.charting.data.Entry
import kotlinx.android.synthetic.main.rates_chart_fragment.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.components.XAxis
import com.route4me.exchangerates.R


class ExchangeRatesChartFragment : ExchangeRatesBaseFragment() {

    private var currencyName: String = ""

    companion object {
        val TAG = "ExchangeRatesChartFragment"
        fun newInstance(): ExchangeRatesChartFragment = ExchangeRatesChartFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragementView = inflater.inflate(R.layout.rates_chart_fragment, container, false)
        if (arguments?.getString(DATA_BUNDLE_CURRENCY_NAME) != null) {
            currencyName = arguments!!.getString(DATA_BUNDLE_CURRENCY_NAME).toString()
        }
        return fragementView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadHistoryData()
    }

    fun loadHistoryData() {
        val loadingAlert = Utils.createDialog(activity!!, "Loading", false)
        loadingAlert.show()
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val calendar = Calendar.getInstance()
        val endDate  = calendar.time
        calendar.add(Calendar.DATE, -7)
        val startDate = calendar.time
        disposables.add(ExchangeRatesAPI.getHistoryFor(currencyName, formatter.format(startDate), formatter.format(endDate))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { ratesMap ->
                    loadingAlert.dismiss()
                    if (ratesMap.isEmpty()) {
                        Utils.createDialog(activity!!, "No exchange rate data is available for the selected currency.").show()
                    } else {
                        setChartData(ratesMap)
                    }
                },
                { error ->
                    loadingAlert.dismiss()
                    Utils.createDialog(activity!!, "Unable to fetch history data").show()
                    Log.e("Error", "### err: " + error)
                }))

    }

    fun setChartData(sortedRatesMap: Map<Date, Float>) {
        val calendar = Calendar.getInstance()
        val now = calendar.timeInMillis
        val xAxis = ratesLineChart.getXAxis()
        xAxis.setPosition(XAxis.XAxisPosition.TOP_INSIDE)
        xAxis.valueFormatter = object : ValueFormatter() {

            private val mFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

            override fun getFormattedValue(value: Float): String {
                return mFormat.format(Date(now + value.toLong()))
            }
        }

        val values = ArrayList<Entry>()

        for (entry in sortedRatesMap) {
            val dateDiff = (entry.key.time - now).toFloat()
            values.add(Entry(dateDiff, entry.value))
        }

        val rateHistorySet = LineDataSet(values, "History")
        rateHistorySet.axisDependency = AxisDependency.LEFT
        rateHistorySet.color = ColorTemplate.getHoloBlue()
        rateHistorySet.valueTextColor = ColorTemplate.getHoloBlue()
        rateHistorySet.lineWidth = 1.5f
        rateHistorySet.setDrawCircles(true)
        rateHistorySet.setDrawValues(true)
        rateHistorySet.fillAlpha = 65
        rateHistorySet.fillColor = ColorTemplate.getHoloBlue()
        rateHistorySet.highLightColor = Color.rgb(244, 117, 117)
        rateHistorySet.setDrawCircleHole(false)

        val data = LineData(rateHistorySet)
        data.setValueTextColor(Color.BLUE)
        data.setValueTextSize(9f)

        ratesLineChart.description.isEnabled = false
        ratesLineChart.setData(data)
        ratesLineChart.notifyDataSetChanged()
        ratesLineChart.invalidate()
    }

}