package com.route4me.exchangerates.ui.fragments

import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable

open class ExchangeRatesBaseFragment : Fragment() {
    protected var disposables: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

}