package com.route4me.exchangerates.api

import com.route4me.exchangerates.model.ExchangeRate
import com.route4me.exchangerates.utils.Utils
import io.reactivex.Observable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

object ExchangeRatesAPI {

    private val ratesAPIService by lazy {
        ExchangeRatesAPIService.create()
    }

    fun getLatestRates(): Observable<List<ExchangeRate>> {
        return ratesAPIService.getRates()
            .map { result ->
            val ratesList: ArrayList<ExchangeRate> = ArrayList()
            for (entry in result.ratesObject.entrySet()) {
                val rateName = entry.key
                ratesList.add(
                    ExchangeRate(
                        name = rateName,
                        value = result.ratesObject.get(rateName).asString.toDouble(),
                        sign = Utils.getCurrencySignBy(rateName)
                    )
                )
            }
            return@map ratesList
        }
    }

    fun getHistoryFor(rateName: String, startDateAsString: String, endDateAsString: String):
            Observable<Map<Date, Float>> {
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        return ratesAPIService.getRatesHistory(startDateAsString, endDateAsString, rateName)
            .map { result ->
                val dateValueMap = HashMap<Date, Float>()
                for (entry in result.ratesObject.entrySet()) {
                    val rateDateString = entry.key
                    val rateDate = formatter.parse(rateDateString)
                    val rateValue = result.ratesObject.get(rateDateString).asJsonObject.get(rateName).asFloat
                    dateValueMap.put(rateDate, rateValue)
                }
                return@map dateValueMap.toSortedMap()
            }
    }

}