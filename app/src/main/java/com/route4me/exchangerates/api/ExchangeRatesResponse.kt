package com.route4me.exchangerates.api

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class ExchangeRatesResponse (

    @SerializedName("rates")
    val ratesObject: JsonObject

)