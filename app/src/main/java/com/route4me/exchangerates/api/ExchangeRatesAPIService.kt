package com.route4me.exchangerates.api

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ExchangeRatesAPIService {

    @GET("latest?base=USD")
    fun getRates(): Observable<ExchangeRatesResponse>

    @GET("history")
    fun getRatesHistory(@Query("start_at")startDate: String, @Query("end_at")endDate: String,
                        @Query("symbols")historyCurrency: String, @Query("base")baseCurrency: String = "USD"):
            Observable<ExchangeRatesResponse>

    companion object {
        fun create(): ExchangeRatesAPIService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.exchangeratesapi.io/")
                .build()

            return retrofit.create(ExchangeRatesAPIService::class.java)
        }
    }

}