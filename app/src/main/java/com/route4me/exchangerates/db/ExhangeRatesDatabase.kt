package com.route4me.exchangerates.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.route4me.exchangerates.model.ExchangeRate

@Database(entities = [ExchangeRate::class], version = 1)
abstract class ExchangeRatesDatabase : RoomDatabase() {
    abstract fun ratesDao(): ExchangeRatesDao

    companion object {
        var INSTANCE: ExchangeRatesDatabase? = null

        fun getAppDataBase(context: Context): ExchangeRatesDatabase? {
            if (INSTANCE == null){
                synchronized(ExchangeRatesDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, ExchangeRatesDatabase::class.java, "ratesDB").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}