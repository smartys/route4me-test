package com.route4me.exchangerates.db

import androidx.room.*
import com.route4me.exchangerates.model.ExchangeRate

@Dao
interface ExchangeRatesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRate(exRate: ExchangeRate)

    @Update
    fun updateRate(exRate: ExchangeRate)

    @Delete
    fun deleteGender(exRate: ExchangeRate)

    @Query("SELECT * FROM ExchangeRate")
    fun getRates(): List<ExchangeRate>

}