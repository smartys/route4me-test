package com.route4me.exchangerates.db

import android.content.Context

object ExchangeRatesDB {
    var db: ExchangeRatesDatabase? = null
    var ratesDao: ExchangeRatesDao? = null

    fun initDB(context: Context) {
        db = ExchangeRatesDatabase.getAppDataBase(context = context)
        ratesDao = db?.ratesDao()
    }

}